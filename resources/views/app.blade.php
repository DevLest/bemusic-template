@extends('common::framework')

@section('angular-styles')
    {{--angular styles begin--}}
		<link rel="stylesheet" href="client/styles.bd1b100edc4a44659421.css">
	{{--angular styles end--}}
@endsection

@section('angular-scripts')
    {{--angular scripts begin--}}
		<script>setTimeout(function() {
        var spinner = document.querySelector('.global-spinner');
        if (spinner) spinner.style.display = 'flex';
    }, 100);</script>
		<script src="client/runtime-es2015.dbe02ba1b5c4381e91fe.js" type="module"></script>
		<script src="client/runtime-es5.dbe02ba1b5c4381e91fe.js" nomodule defer></script>
		<script src="client/polyfills-es5.85a146d3b567a2ddbb57.js" nomodule defer></script>
		<script src="client/polyfills-es2015.394385f3043280af1d7f.js" type="module"></script>
		<script src="client/main-es2015.158f38c9b10e1e0cd7d5.js" type="module"></script>
		<script src="client/main-es5.158f38c9b10e1e0cd7d5.js" nomodule defer></script>
	{{--angular scripts end--}}
@endsection
